from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}&{state}&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)

    parsed_response = json.loads(response.text)
    photo = {
        "picture_url": parsed_response["photos"][0]["url"],
    }
    return photo

def get_weather(city, state):
    gc_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=5&appid={OPEN_WEATHER_API_KEY}"
    gc_response = requests.get(gc_url)

    gc_parsed_response = json.loads(gc_response.text)
    lat_long = {
        "latitude": gc_parsed_response[0]["lat"],
        "longitude": gc_parsed_response[0]["lon"]
    }

    cw_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat_long['latitude']}&lon={lat_long['longitude']}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    cw_response = requests.get(cw_url)

    cw_parsed_response = json.loads(cw_response.text)
    weather = {
        "temp": cw_parsed_response["main"]["temp"],
        "description": cw_parsed_response["weather"][0]["description"]
    }
    return weather
